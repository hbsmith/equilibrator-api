"""A test module for models."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest
from path import Path

from equilibrator_api.model import Bounds, StoichiometricModel


@pytest.fixture(scope="module")
def test_dir() -> Path:
    """Get the test directory."""
    return Path(__file__).abspath().parent


@pytest.mark.parametrize(
    "sbtab_fname",
    [("ccm_without_dgs.tsv"), ("ccm_with_dgs.tsv"), ("ccm_with_keq.tsv")],
)
def test_pathway(sbtab_fname, test_dir, comp_contribution):
    """Test reading a StoichiometricModel from SBtab."""
    filename = str(test_dir / sbtab_fname)

    model = StoichiometricModel.from_sbtab(
        filename, comp_contrib=comp_contribution
    )

    assert model.Nr == 30
    assert model.Nc == 40


def test_default_bounds(comp_contribution):
    """Test the default bounds."""
    bounds = Bounds.get_default_bounds(comp_contribution)

    bounds.check_bounds()
