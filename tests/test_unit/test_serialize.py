"""unit test for balance with oxidation function."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest

from equilibrator_api import Q_


def test_serialize(reaction_dict, comp_contribution):
    """Test the serialize function of PhasedRection."""
    atp_compound = comp_contribution.get_compound("kegg:C00002")
    atp_hydrolysis = reaction_dict["atpase"]

    atp_hydrolysis.set_abundance(atp_compound, Q_("10 mM"))

    ser = atp_hydrolysis.serialize()

    assert ser[0]["inchi_key"] == "ZKHQWZAMYRWXGA-KQYNXXCUSA-J"  # ATP
    assert ser[0]["phase"] == "aqueous"
    assert ser[0]["coefficient"] == -1
    assert len(ser[0]["microspecies"]) == 13

    assert ser[1]["inchi"] == "InChI=1S/H2O/h1H2"  # H2O
    assert ser[1]["phase"] == "liquid"
    assert len(ser[1]["microspecies"]) == 1

    assert ser[2]["inchi_key"] == "XTWYTFMLZFPYCI-KQYNXXCUSA-K"  # ADP
    assert len(ser[2]["microspecies"]) == 11


@pytest.mark.parametrize(
    "formula, reference",
    [
        ("kegg:C00008 + kegg:C00009 = kegg:C00001 + kegg:C00002", "atpase"),
        (
            "metanetx.chemical:MNXM3 + metanetx.chemical:MNXM2 = "
            "metanetx.chemical:MNXM9 + metanetx.chemical:MNXM7",
            "atpase",
        ),
        (
            "2 kegg:C00002 + 2 kegg:C00001 = 2 kegg:C00009 + 2 kegg:C00008",
            "atpase",
        ),
    ],
)
def test_md5(formula, reference, reaction_dict, comp_contribution):
    """Test the hash function."""
    rxn = comp_contribution.parse_reaction_formula(formula)
    print(rxn._hashable_reactants(rxn.sparse_with_phases))
    assert rxn.hash_md5() == reaction_dict[reference].hash_md5()
